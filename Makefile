# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/09/11 11:31:40 by tstephen          #+#    #+#              #
#    Updated: 2018/09/11 14:28:14 by tstephen         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = corewar

SRC_PREFIX = ./srcs/

SOURCES = main.c \
		  utils.c

C_SRCS = $(SOURCES:.c=.o)

C_FLAGS = -Wall -Wextra -Werror

$(NAME):
	make -C ./libft/
	gcc $(C_FLAGS) $(addprefix $(SRC_PREFIX), $(SOURCES)) ./libft/libft.a -o $(NAME)

all: $(NAME)

clean:
	make clean -C ./libft/
	/bin/rm -rf $(C_SRCS)

fclean: clean
	/bin/rm -rf $(NAME)
	/bin/rm -rf ./libft/libft.a

re: fclean all