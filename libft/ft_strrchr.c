/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 10:56:46 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/20 12:13:52 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*ptr;
	int		counter;

	counter = 0;
	ptr = (char*)s;
	while (*ptr)
	{
		ptr++;
		counter++;
	}
	while (counter >= 0)
	{
		if (*ptr == (char)c)
			return (ptr);
		ptr--;
		counter--;
	}
	return (NULL);
}
