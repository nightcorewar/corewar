/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/01 09:47:45 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/25 13:27:03 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned	int		i;
	unsigned	int		counter;
	char				*result;

	i = 0;
	counter = 0;
	if (!s)
		return (NULL);
	while (s[i])
	{
		i++;
	}
	if (!(result = ft_strnew(i)))
		return (NULL);
	while (counter < i)
	{
		result[counter] = (*f)(counter, s[counter]);
		counter++;
	}
	return (result);
}
