/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/23 21:11:07 by tstephen          #+#    #+#             */
/*   Updated: 2018/07/23 21:12:41 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long		ft_atol(const char *str)
{
	long		nb;
	int		sign;

	nb = 0;
	sign = 1;
	while (((*str > 8 && *str < 14) || *str == 32) && *str)
		str++;
	if (*str == '-')
	{
		sign = -1;
		str++;
	}
	if (*str == '+' && sign != -1)
		str++;
	while (*str && (*str >= '0' && *str <= '9'))
	{
		nb *= 10;
		nb += *str - '0';
		str++;
	}
	return (nb * sign);
}
