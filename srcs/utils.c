/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/11 10:46:22 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/11 14:27:13 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/utils.h"

char			*ft_intrev(char *str, size_t size)
{
	char	*temp;
	size_t	itr;
	size_t	itr2;

	temp = ft_memalloc(size);
	itr = 0;
	itr2 = size - 1;
	while (itr < size)
	{
		temp[itr2] = str[itr];
		itr++;
		itr2--;
	}
	itr = 0;
	while (itr < size)
	{
		str[itr] = temp[itr];
		itr++;
	}
	free(temp);
	return (str);
}

static void		put2hex(unsigned char num, size_t turn)
{
	static char	*hex_val = "0123456789abcdef";

	if (turn == 3)
		return ;
	put2hex(num / 16, turn + 1);
	ft_putchar(hex_val[num % 16]);
}

void			ft_put2hex(unsigned char num)
{
	if (num == 0)
		ft_putstr("00");
	else
		put2hex(num, 1);
}

void			ft_dump_grid(t_game *game)
{

}
