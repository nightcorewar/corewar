/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/10 09:04:01 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/11 14:08:20 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/op.h"
#include "../includes/champion_info.h"
#include "../includes/utils.h"
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

t_header		*champion_info(int fd)
{
	t_header	*champion;
	char		c;

	c = 0;
	champion = (t_header*)ft_memalloc(sizeof(t_header));
	champion->magic = 0;
	read(fd, &(champion->magic), sizeof(unsigned int));
	ft_intrev((char*)&(champion->magic), sizeof(unsigned int));
	read(fd, champion->prog_name, PROG_NAME_LENGTH);
	while (c == 0)
		read(fd, &c, 1);
	while (!ft_isprint(c))
		read(fd, &c, 1);
	champion->prog_name[PROG_NAME_LENGTH] = '\0';
	champion->comment[0] = c;
	read(fd, &(champion->comment[1]), COMMENT_LENGTH - 1);
	champion->comment[COMMENT_LENGTH] = '\0';
	lseek(fd, 0x890, SEEK_SET);
	champion->prog_size = read(fd, champion->instructions, CHAMP_MAX_SIZE);
	return (champion);
}

int				main(int argc, char **argv)
{
	int				fd[argc - 1];
	int				i;
	t_header		*header;

	i = 1;
	while (i < argc)
	{
		fd[i - 1] = open(argv[i], O_RDONLY);
		i++;
	}
	header = champion_info(fd[0]);
	printf("MAGIC NUMBER: %u\nNAME: %s\nCOMMENT: %s\n", header->magic, header->prog_name, header->comment);
	i = 0;
	printf("\nINSTRUCTIONS\n");
	while (i < (int)header->prog_size)
	{
		ft_put2hex(header->instructions[i]);
		ft_putchar(' ');
		i++;
	}
	return (0);
}
